<nav class="purple lighten-1" role="navigation">
  <div class="nav-wrapper container">
    <a id="logo-container" href="#" class="brand-logo white-text">
      Starship Motors
    </a>
    <ul class="right hide-on-med-and-down">
      <li><a href="index.php" class="white-text">Home</a></li>
      <li><a href="consultar.php" class="white-text">Consultar</a></li>
      <li><a href="adicionar.php" class="white-text">Adicionar</a></li>
    </ul>

    <ul id="nav-mobile" class="sidenav">
      <li><a href="index.php" class="black-text">Home</a></li>
      <li><a href="consultar.php" class="black-text">Consultar</a></li>
      <li><a href="adicionar.php" class="black-text">Adicionar</a></li>
    </ul>
    <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
  </div>
</nav>