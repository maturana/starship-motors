<?php
include_once 'config/database.php';

$carros = mysqli_query($conn, "SELECT * FROM carros");
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Consultar</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <?php include_once 'layout/header.php'; ?>

  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
      <br><br>
      <div class="row center">
        <div class="col s12 m12 l12 xl12">
          <div class="col s12 m12 l12 xl12">
            <h3 class="light">Estoque de carros</h3>
            <?php
            if (mysqli_num_rows($carros) > 0) {
              echo '<table class="stripped">
                <thead>
                  <tr>
                    <th>Marca</th>
                    <th>Modelo</th>
                    <th>Descrição</th>
                    <th>Modelo/Fabricação</th>
                    <th>Cor</th>
                    <th>Placa</th>
                    <th>Preço</th>
                  </tr>
                </thead>
              <tbody>';
              while ($carro = mysqli_fetch_assoc($carros)) {
                echo "<tr>";
                echo "<td>" . $carro["marca"] . "</td>";
                echo "<td>" . $carro["modelo"] . "</td>";
                echo "<td>" . $carro["descricao"] . "</td>";
                echo "<td>" . $carro["ano"] . "</td>";
                echo "<td>" . $carro["cor"] . "</td>";
                echo "<td>" . $carro["placa"] . "</td>";
                echo "<td>" . $carro["valor"] . "</td>";
                echo '<td><a href="atualizar.php?id=' . $carro["id"] . '" class="btn-floating orange"><i class="material-icons">edit</i></a></td>';
                echo '<td><a href="#modal-' . $carro["id"] . '" class="btn-floating modal-trigger red"><i class="material-icons">delete</i></a></td>';
                echo "</tr>";

                echo '<div id="modal-' . $carro["id"] . '" class="modal">
                  <div class="modal-content">
                    <h4>Deseja mesmo excluir ' . $carro["marca"] . ' ' . $carro["modelo"] . '?</h4>
                  </div>
                  <div class="modal-footer">
                    <form action="controllers/delete.php?id=' . $carro["id"] . '" method="post">
                      <button type="submit" id="deletar" name="deletar" class="btn red">
                        Sim
                      </button>
                    </form>
                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">
                      Não
                    </a>
                  </div>
                </div>';
              }
              echo '</tbody>';
              echo '</table>';
            } else {
              echo '<h4>Não há carros cadastrados no sistema!</h4>';
              echo '<a href="adicionar.php"><button class="btn">Cadastrar novo carro</button></a>';
            }
            ?>
          </div>
        </div>
      </div>
      <br><br>

    </div>
  </div>

  <?php include_once 'layout/footer.php'; ?>

  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>

  </body>
  <script>
    M.AutoInit();
  </script>
</html>
