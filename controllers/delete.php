<?php

session_start();

include_once '../config/database.php';

if (isset($_POST["deletar"])) {
  $id = (int) mysqli_escape_string($conn, $_GET["id"]);

  if (mysqli_query($conn, "DELETE FROM carros WHERE id = $id")) {
    header('Location: ../consultar.php');
  } else {
    die("Erro: carro de id '$id' não foi encontrado!");
  }
}
?>